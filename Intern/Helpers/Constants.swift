//
//  Constants.swift
//  Intern
//
//  Created by Harish Gopinath on 2017-09-11.
//  Copyright © 2017 Harish. All rights reserved.
//

import UIKit

struct Constants {
    
    static let isIpad = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad
    
    struct FontSize {
        static let Button: CGFloat = 22
        static let RegularText: CGFloat = 14
        static let TitleText: CGFloat = 18
        static let ReportValueText: CGFloat = 22
    }
    
    struct Color {
        static let AppTheme: UIColor = UIColor(colorLiteralRed: 33/255, green: 47/255, blue: 118/255, alpha: 1.0)
        static let TitleText: UIColor = UIColor.white
    }
    
    struct Number {
        static let CornerRadius: CGFloat = 4
        static let ShortAnimate: TimeInterval = 0.1
    }
    
    struct Image {
        static let NoRecord: String = "NoRecord"
        static let Product: String = "Product"
        static let Customer: String = "Customer"
    }
    
}
