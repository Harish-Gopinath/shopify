//
//  LineItems.swift
//  Intern
//
//  Created by Harish Gopinath on 2017-09-11.
//  Copyright © 2017 Harish. All rights reserved.
//

import ObjectMapper

class LineItems: Mappable {
    
    var title: String!
    var quantity: Int!
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        title <- map["title"]
        quantity <- map["quantity"]
    }
}
