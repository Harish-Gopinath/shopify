//
//  Order.swift
//  Intern
//
//  Created by Harish Gopinath on 2017-09-11.
//  Copyright © 2017 Harish. All rights reserved.
//

import ObjectMapper

class Order: Mappable {
    
    var email: String!
    var totalPrice: String! //Because of the issue with the ObjectMapper used. Else would be of type Double 
    var lineItems: [LineItems]!
    var currency: String!
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        email <- map["email"]
        totalPrice <- map["total_price"]
        lineItems <- map["line_items"]
        currency <- map["currency"]
    }
    
}
