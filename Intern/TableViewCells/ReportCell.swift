//
//  ReportCell.swift
//  Intern
//
//  Created by Harish Gopinath on 2017-09-11.
//  Copyright © 2017 Harish. All rights reserved.
//

import UIKit

class ReportCell: UITableViewCell {
    
    @IBOutlet weak var captionLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        captionLabel.applyCellCaptionTheme()
        valueLabel.applyCellValueTheme()
    }
    
    func updateData(caption: String, value: String) {
        captionLabel.text = caption.length > 0 ? caption : "unknown"
        valueLabel.text = value
    }
    
}
