//
//  ProductReportViewController.swift
//  Intern
//
//  Created by Harish Gopinath on 2017-09-12.
//  Copyright © 2017 Harish. All rights reserved.
//

import UIKit

typealias ProductTuple = (name: String, totalSoldQuantity: Int)

class ProductReportViewController: ReportViewController {
    
    fileprivate var productRecords: [ProductTuple] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // Pull to refresh can be done to load new data. Not implemented here. Refunds are not considered as I believe its not in scope for this problem (and test data doesnt have either)
    override func populateData() {
        view.makeToastActivity(.center)
        SyncEngine.sharedInstance.getOrders(success: { (orders) in
            var productReport: [String: Int] = [:]
            for order in orders {
                for lineItem in order.lineItems {
                    productReport[lineItem.title] = (productReport[lineItem.title] ?? 0) + lineItem.quantity
                }
            }
            for (name, totalSoldQuantity) in productReport {
                self.productRecords.append(ProductTuple(name, totalSoldQuantity))
            }
            self.productRecords = self.productRecords.sorted(by: { $0.0 < $1.0 })
            self.isLoading = false
            self.tableView.reloadData()
            self.view.hideToastActivity()
        }) { (failureReason) in
            self.view.showMessage(message: failureReason)
            self.isLoading = false
            self.tableView.reloadData()
            self.view.hideToastActivity()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}


// TableView methods
extension ProductReportViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productRecords.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! ReportCell
        let detailViewController = DetailViewController.instantiateFrom(appStoryboard: .Main)
        detailViewController.updateDefaults(viewTitle: cell.captionLabel!.text!, title: "Total Sold Quantity", value: cell.detailTextLabel!.text!)
        navigationController?.pushViewController(detailViewController, animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reportCell) as! ReportCell
        let record = productRecords[indexPath.row]
        cell.updateData(caption: record.name, value: String(record.totalSoldQuantity))
        return cell
    }
    
}
