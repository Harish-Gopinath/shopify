//
//  DetailViewController.swift
//  Intern
//
//  Created by Harish Gopinath on 2017-09-12.
//  Copyright © 2017 Harish. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    
    private var viewTitleText: String = ""
    private var titleText: String = ""
    private var valueText: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = viewTitleText
        containerView.applyContainerAppTheme()
        titleLabel.applyReportTitleTheme()
        valueLabel.applyReportValueTheme()
        titleLabel.text = titleText
        valueLabel.text = valueText
    }
    
    func updateDefaults(viewTitle: String, title: String, value: String) {
        viewTitleText = viewTitle
        titleText = title
        valueText = value
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
