//
//  CustomerReportViewController.swift
//  Intern
//
//  Created by Harish Gopinath on 2017-09-12.
//  Copyright © 2017 Harish. All rights reserved.
//

import UIKit

typealias CustomerTuple = (name: String, totalSpent: Double)

class CustomerReportViewController: ReportViewController {
    
    fileprivate var customerRecords: [CustomerTuple] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // Pull to refresh can be done to load new data. Not implemented here. Refunds are not considered as I believe its not in scope for this problem (and test data doesnt have either)
    override func populateData() {
        view.makeToastActivity(.center)
        SyncEngine.sharedInstance.getOrders(success: { (orders) in
            var customerReport: [String: Double] = [:]
            for order in orders {
                customerReport[order.email] = (customerReport[order.email] ?? 0) + Double(order.totalPrice)!
            }
            for (name, totalSpent) in customerReport {
                self.customerRecords.append(CustomerTuple(name, totalSpent))
            }
            self.customerRecords = self.customerRecords.sorted(by: { $0.0 < $1.0 })
            self.isLoading = false
            self.tableView.reloadData()
            self.view.hideToastActivity()
        }) { (failureReason) in
            self.view.showMessage(message: failureReason)
            self.isLoading = false
            self.tableView.reloadData()
            self.view.hideToastActivity()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}


// TableView methods
extension CustomerReportViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return customerRecords.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! ReportCell
        let detailViewController = DetailViewController.instantiateFrom(appStoryboard: .Main)
        detailViewController.updateDefaults(viewTitle: cell.captionLabel!.text!, title: "Total Amount Spent", value: cell.detailTextLabel!.text!)
        navigationController?.pushViewController(detailViewController, animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reportCell) as! ReportCell
        let record = customerRecords[indexPath.row]
        cell.updateData(caption: record.name, value: record.totalSpent.getInCurrency())
        return cell
    }
    
}
