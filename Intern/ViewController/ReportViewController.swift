//
//  ReportViewController.swift
//  Intern
//
//  Created by Harish Gopinath on 2017-09-11.
//  Copyright © 2017 Harish. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

class ReportViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    // Inherited controllers need to indicate the completion of network request. Used to make a decision of showing empty data set
    var isLoading: Bool = true
    let reportCell = "ReportCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        automaticallyAdjustsScrollViewInsets = false
        applyTabBarControllerTheme()
        applyNavigationControllerTheme()
        tableView.tableFooterView = UIView()
        tableView.sectionHeaderHeight = 0
        tableView.sectionFooterHeight = 0
        populateData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let selectedRow = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: selectedRow, animated: true)
        }
    }
    
    func populateData() {
        // Should override by inherited controllers to populate data into table
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}


// Empty dataset visual in tableView
extension ReportViewController: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        return UIImage(named: Constants.Image.NoRecord)!
    }
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView!) -> Bool {
        return !isLoading
    }
    
    func emptyDataSet(_ scrollView: UIScrollView!, didTap button: UIButton!) {
        populateData()
    }
    
    func buttonTitle(forEmptyDataSet scrollView: UIScrollView, for state: UIControlState) -> NSAttributedString? {
        return NSAttributedString(string: "Tap to refresh!", attributes: [NSForegroundColorAttributeName: Constants.Color.AppTheme, NSFontAttributeName: UIFont.systemFont(ofSize: Constants.FontSize.Button)])
    }
    
}
