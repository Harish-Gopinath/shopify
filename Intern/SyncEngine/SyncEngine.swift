//
//  SyncEngine.swift
//  Intern
//
//  Created by Harish Gopinath on 2017-09-11.
//  Copyright © 2017 Harish. All rights reserved.
//

import Alamofire
import ObjectMapper

typealias DataObjects = [String : [[String: Any]]]

class SyncEngine {
 
    let orderKey: String = "orders"
    let orderEndpoint: String = "https://shopicruit.myshopify.com/admin/orders.json?fields=email,total_price,line_items,currency&page=1&access_token=c32313df0d0ef512ca64d5b336a0d7c6"
    
    static let sharedInstance = SyncEngine()
    private init() {}

    func getOrders(success: @escaping ([Order]) -> Void, failure: @escaping (String) -> Void) {
        Alamofire.request(orderEndpoint, method: .get).responseJSON { response in
            if let data = response.result.value as? DataObjects {
                success(Mapper<Order>().mapArray(JSONArray: data[self.orderKey]!))
            } else {
                failure(response.error?.localizedDescription ?? "Failed to map data from server")
            }
        }
    }
    
}
