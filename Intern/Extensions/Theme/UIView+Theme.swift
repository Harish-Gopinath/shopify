//
//  UIView+Theme.swift
//  Intern
//
//  Created by Harish Gopinath on 2017-09-12.
//  Copyright © 2017 Harish. All rights reserved.
//

import UIKit

extension UIView {
    
    func applyContainerAppTheme() {
        backgroundColor = Constants.Color.AppTheme
        applyCornerRadius()
    }
    
    func applyCornerRadius() {
        clipsToBounds = true
        layer.cornerRadius = Constants.Number.CornerRadius
    }
    
}
