//
//  UIViewController+Theme.swift
//  Intern
//
//  Created by Harish Gopinath on 2017-09-12.
//  Copyright © 2017 Harish. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func applyTabBarControllerTheme() {
        tabBarController?.tabBar.tintColor = Constants.Color.AppTheme
    }
    
    func applyNavigationControllerTheme() {
        navigationController?.navigationBar.tintColor = Constants.Color.TitleText
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:Constants.Color.TitleText]
        navigationController?.navigationBar.barTintColor = Constants.Color.AppTheme
    }

}
