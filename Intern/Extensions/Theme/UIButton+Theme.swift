//
//  UIButton+Theme.swift
//  Intern
//
//  Created by Harish Gopinath on 2017-09-11.
//  Copyright © 2017 Harish. All rights reserved.
//

import UIKit

extension UIButton {
    
    override open var isHighlighted: Bool {
        didSet {
            if (isHighlighted) {
                self.alpha = 0.4
            }else {
                UIView.animate(withDuration: 0.3, animations: { () -> Void in
                    self.alpha = 1
                })
            }
        }
    }
    
}
