//
//  UILabel+Theme.swift
//  Intern
//
//  Created by Harish Gopinath on 2017-09-12.
//  Copyright © 2017 Harish. All rights reserved.
//

import UIKit

extension UILabel {
    
    func applyCellCaptionTheme() {
        font = UIFont.systemFont(ofSize: Constants.FontSize.RegularText)
    }
    
    func applyCellValueTheme() {
        font = UIFont.systemFont(ofSize: Constants.FontSize.RegularText)
        textColor = Constants.Color.AppTheme
    }
    
    func applyReportTitleTheme() {
        font = UIFont.systemFont(ofSize: Constants.FontSize.TitleText)
        textColor = Constants.Color.TitleText
    }
    
    func applyReportValueTheme() {
        font = UIFont.systemFont(ofSize: Constants.FontSize.ReportValueText)
        textColor = Constants.Color.TitleText
    }
    
}
