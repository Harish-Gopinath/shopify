//
//  Toast.swift
//  Intern
//
//  Created by Harish Gopinath on 2017-09-11.
//  Copyright © 2017 Harish. All rights reserved.
//

import Foundation
import Toast_Swift

extension ToastManager {
    
    func initialSetup() {
        queueEnabled = false
        var customStyle = ToastStyle()
        customStyle.cornerRadius = Constants.Number.CornerRadius
        customStyle.backgroundColor = Constants.Color.AppTheme.withAlphaComponent(0.8)
        customStyle.messageFont = UIFont.systemFont(ofSize: Constants.FontSize.RegularText)
        customStyle.displayShadow = true
        if Constants.isIpad {
            customStyle.horizontalPadding = 40
            customStyle.verticalPadding = 40
        } else {
            customStyle.horizontalPadding = 20
            customStyle.verticalPadding = 20
        }
        ToastManager.shared.style = customStyle
    }
    
}
