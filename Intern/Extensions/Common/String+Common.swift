//
//  String+Common.swift
//  Intern
//
//  Created by Harish Gopinath on 2017-09-11.
//  Copyright © 2017 Harish. All rights reserved.
//

import Foundation

extension String {
    
    var wordCount: Int {
        return self.components(separatedBy: CharacterSet.whitespaces).count
    }
    
    var length: Int {
        return self.characters.count
    }
    
}
