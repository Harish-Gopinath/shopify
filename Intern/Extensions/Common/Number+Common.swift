//
//  Number+Common.swift
//  Intern
//
//  Created by Harish Gopinath on 2017-09-12.
//  Copyright © 2017 Harish. All rights reserved.
//

import Foundation

extension Double {
    
    func getInCurrency() -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.currencyCode = "CAD" // This can be done based on customer locale in orders api
        return formatter.string(from: self as NSNumber)!
    }
    
}
