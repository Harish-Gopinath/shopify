//
//  UIView+Common.swift
//  Intern
//
//  Created by Harish Gopinath on 2017-09-11.
//  Copyright © 2017 Harish. All rights reserved.
//

import UIKit

extension UIView {
    
    func showMessage(message: String) {
        let wordCount = Double(message.wordCount)
        let duration = 1 + (wordCount * 0.3)
        self.makeToast(message, duration: duration, position: .center)
    }
    
}
